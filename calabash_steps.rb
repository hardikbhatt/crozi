Given(/^the app has launched$/) do
end

# Then(/^I touch SKIP$/) do
# element = query("* marked:'activity_help_skip_textview'").first
# puts element
# wait_poll(:until_exists => "* marked:'activity_help_skip_textview'", :timeout => 20) do
# #scroll("tableView",:down)
# end
# touch(element)
# end

# Home page

Then(/^I press Home Menu button$/) do
		element1 = query("ImageButton")
		touch(element1)
end

Then(/^I should see User Profile Name$/) do
		puts element_exists("* marked:'profile_name_text'")
end

# Open Home Menu

Then(/^I press Home Button$/) do
		element2 = query("AppCompatTextView text:'Home'")
		touch(element2)
end

Then(/^I should see Home Page$/) do
		puts element_exists("* marked:'action_search'")
		sleep(5)
end

# Checking notification menu from sidebar

Then(/^I press Notifications Button$/) do
		element3 = query("AppCompatTextView text:'Notifications'")
		touch(element3)
end

Then(/^I should see Notifications Page$/) do
	wait_poll(:until_exists => "* marked:'followUserImage'", :timeout => 20) do
	end
		puts element_exists("* marked:'followUserImage'")
		sleep(5)
end

# Checking Find Friends menu from sidebar

Then(/^I press Find Friends Button$/) do
		element4 = query("AppCompatTextView text:'Find Friends'")
		touch(element4)
end

Then(/^I should see Find Friends Page$/) do
    wait_poll(:until_exists => "* marked:'followUserImage'", :timeout => 20) do
	end
		puts element_exists("* marked:'followUserImage'")
		sleep(5)
end

Then(/^I press Facebook Button$/) do
    wait_poll(:until_exists => "* text:'Facebook'", :timeout => 20) do
	end
		element5 = query("* text:'Facebook'")
		touch(element5)
end

Then(/^I should see Facebook User$/) do
    wait_poll(:until_exists => "* marked:'card_view'", :timeout => 20) do
	end
		puts element_exists("* marked:'card_view'")
		sleep(5)
end

# Checking Settings menu from sidebar

Then(/^I press Settings Button$/) do
		element6 = query("AppCompatTextView text:'Settings'")
		touch(element6)
end

Then(/^I should see Settings Page$/) do
	wait_poll(:until_exists => "* marked:'card_view'", :timeout => 20) do
	end
		puts element_exists("* marked:'notificationSwitch'")
		sleep(5)
end

# Open Home Page

Then(/^I should see selected category as Everything$/) do
	wait_poll(:until_exists => "* text:'Everything'", :timeout => 20) do
	end
		element7 = "Everything"
		nowCategory = query("* marked:'selectedCategoryTV'", :text).first
		puts "Now selected category is: #{(nowCategory).eql? (element7)}"
end

Then(/^I press category dropdown$/) do
    wait_poll(:until_exists => "* marked:'categoryDownImage'", :timeout => 20) do
	end
		element8 = query("AppCompatImageView marked:'categoryDownImage'")
		touch(element8)
end

Then(/^I press any category name/) do
    wait_poll(:until_exists => "* marked:'categoryDownImage'", :timeout => 20) do
	end
		element9 = query("AppCompatTextView text:'Art'")
		touch(element9)
end

Then(/^I should see the selected category page$/) do
    wait_poll(:until_exists => "* text:'Art'", :timeout => 20) do
	end
		element10 = "Art"
		nowCategory = query("* marked:'selectedCategoryTV'", :text).first
		puts "Now selected category is: #{(nowCategory).eql? (element10)}"
end

# Checking User story page

Then(/^I press UserName$/) do
    wait_poll(:until_exists => "* marked:'storyUserNameTV'", :timeout => 20) do
	end
		element11 = query("* android.widget.TextView marked:'storyUserNameTV'")
		touch(element11)
end

Then(/^I should see user story page$/) do
    wait_poll(:until_exists => "* text:'Details'", :timeout => 20) do
	end
		element12="Details"
		nowMyPage = query("* android.widget.TextView'", :text).first
		puts "Displayed Page is story page : #{(nowMyPage).eql? (element12)}"
end

# Checking Like and comment functionality on user story page

previousLike=0

Then(/^I see like count$/) do
    wait_poll(:until_exists => "* marked:'likeNumberTV'", :timeout => 20) do
	end
		previousLike = query("* marked:'likeNumberTV'", :text).first
		puts "Previous Like count is: #{(previousLike)}"
end

Then(/^I press like button$/) do
    wait_poll(:until_exists => "* marked:'likeIV'", :timeout => 20) do
	end
		element13 = query("* marked:'likeIV'").first
		touch(element13)
end

Then(/^I check the like count$/) do
    wait_poll(:until_exists => "* marked:'likeNumberTV'", :timeout => 20) do
	end
		nowLike = query("* marked:'likeNumberTV'", :text).first
		puts "Like count is: #{(nowLike.to_i).eql? (previousLike.to_i+1)}"
end

Then(/^I press on comment icon$/) do
    wait_poll(:until_exists => "* marked:'commentLayout'", :timeout => 20) do
	end
		element14 = query("* marked:'commentLayout'").first
		touch(element14)
end

Then(/^I should see comment page$/) do
    wait_poll(:until_exists => "* marked:'commentET'", :timeout => 20) do
	end
		check_element_exists("* marked:'commentET' enabled:'true'")
end

Then(/^I add the comment and press enter$/) do
	wait_poll(:until_exists => "* marked:'commentET'", :timeout => 20) do
	end
		element15 = query("* marked:'commentET'")
		touch(element15)
		keyboard_enter_text("Hello World")
		element16 = query("* marked:'sendCommentBtn'")
		touch(element16)
end

Then(/^the comment should get added$/) do
    wait_poll(:until_exists => "* marked:'userCommentTV'", :timeout => 20) do
	end
		check_element_exists("* marked:'userCommentTV'")
end

# Checking of story added to collection

Then(/^I press collection$/) do
	wait_poll(:until_exists => "* marked:'collectionLayout'", :timeout => 20) do
	end
		element17 = query("* marked:'collectionLayout'")
		touch(element17)
end

Then(/^I should see collection popup$/) do
	wait_poll(:until_exists => "* marked:'collectionCardView'", :timeout => 20) do
	end
		check_element_exists("* marked:'collectionCardView'")
end

Then(/^I press on create new collection icon$/) do
	wait_poll(:until_exists => "* marked:'createNewCollectionLayout'", :timeout => 20) do
	end
		element18 = query("* marked:'createNewCollectionLayout'")
		touch(element18)
end

Then(/^I should see create new collection popup$/) do
	wait_poll(:until_exists => "* marked:'newCollectionCardView'", :timeout => 20) do
	end
		check_element_exists("* marked:'newCollectionCardView'")
end

Then(/^I add name of collection and press save$/) do
    wait_poll(:until_exists => "* marked:'addNewColectionET'", :timeout => 20) do
	end
		element19 = query("* marked:'addNewColectionET'")
		touch(element19)
		keyboard_enter_text("Test New1" + rand(0..500).to_s)
		element20 = query("* marked:'saveBtn'")
		touch(element20)
end

Then(/^I should see newly created collection$/) do
	wait_poll(:until_exists => "* marked:'filterText'", :timeout => 20) do
	end
		check_element_exists("* marked:'filterText' text:'Test New1'")
end

Then(/^I check which collection to select$/) do
	wait_poll(:until_exists => "* marked:'categoty_item_layout'", :timeout => 20) do
	end
		check_element_exists("* marked:'categoty_item_layout'")

 end

Then(/^I press on done$/) do
    wait_poll(:until_exists => "* marked:'doneBtn'", :timeout => 50) do
    end
        element21 = query("* marked:'doneBtn'")
        touch(element21)
end

Then(/^I press on user profile$/) do
	wait_poll(:until_exists => "* marked:'profile_image'", :timeout => 20) do
	end
		element22 = query("* marked:'profile_image'")
		touch(element22)
end
    
Then(/^I see user profile page$/) do
	wait_poll(:until_exists => "* marked:'user_name'", :timeout => 20) do
	end
		check_element_exists("* marked:'user_name'")   
end

Then(/^I press on collection tab$/) do
	wait_poll(:until_exists => "* marked:'profile_image'", :timeout => 20) do
	end
		touch("* {text CONTAINS 'COLLECTIONS'}")
end

Then(/^I press on collection name$/) do
	wait_poll(:until_exists => "* marked:'collectionLayout'", :timeout => 20) do
	end
		element23 = query("* marked:'collectionLayout'")   
		touch(element23)
end

Then(/^I see my user stories in collection$/) do
	wait_poll(:until_exists => "* marked:'container'", :timeout => 20) do
	end
		check_element_exists("* marked:'container'")   
end  
  
# Checking the other options available in story

Then(/^I should click on other options$/) do
	wait_poll(:until_exists => "* marked:'moreOptionImageView'", :timeout => 20) do
	end
		element24 = query("* marked:'moreOptionImageView'")
		touch(element24)
end
 
Then(/^I should see options popup$/) do
	wait_poll(:until_exists => "* marked:'moreOptionCardView'", :timeout => 20) do
	end
		check_element_exists("* marked:'moreOptionCardView'")  
end   
  
Then(/^I should click on Reports$/) do
	wait_poll(:until_exists => "* marked:'reportLayout'", :timeout => 20) do
	end
		element25 = query("* marked:'reportLayout'")
		touch(element25)
end
 
Then(/^I should see Reports popup$/) do
	wait_poll(:until_exists => "* marked:'reportCardView'", :timeout => 20) do
	end
		check_element_exists("* marked:'reportCardView'")  
end
  
Then(/^I should select any one reason to report and press on done$/) do
    for i in 1..7
        element26 = query("* marked:'rd#{i}'")
        touch(element26)
        if i==7 then 
            if element_exists("* marked:'otherReportET'")
                sleep(2)
                keyboard_enter_text("Test New1")
                touch("* marked:'otherDoneBtn'")
            end
        end
   end
end

# Create a Story

# Then(/^I should click on add item$/) do
#	wait_poll(:until_exists => "* marked:'fab_expand_menu_button'", :timeout => 20) do
#	end
#		element27 = query("* marked:'fab_expand_menu_button'")
#		touch(element27)
# end

# Then(/^I should click on add photo$/) do
#	wait_poll(:until_exists => "* marked:'action_image'", :timeout => 20) do
#	end
#		element28 = query("* marked:'action_image'")
#		touch(element28)
# end

# Then(/^I should click on add video$/) do
#	wait_poll(:until_exists => "* marked:'action_video'", :timeout => 20) do
#	end
#		element29 = query("* marked:'action_video'")
#		touch(element29)
# end

# Then(/^I should click on add embed$/) do
#	wait_poll(:until_exists => "* marked:'action_embed'", :timeout => 20) do
#	end
#		element30 = query("* marked:'action_embed'")
#		touch(element30)
# end

# Search Options

Then(/^I should click on search$/) do
	wait_poll(:until_exists => "* marked:'action_search'", :timeout => 20) do
	end
		element31 = query("* marked:'action_search'")
		touch(element31)
end

Then(/^I search and press enter$/) do
	wait_poll(:until_exists => "* marked:'tempToolbar'", :timeout => 20) do
	end
		element32 = query("* marked:'tempToolbar'")
		touch(element32)
		keyboard_enter_text("Hello World")
		press_user_action_button
end

Then(/^I should click on setting$/) do
	wait_poll(:until_exists => "* marked:'checkIV'", :timeout => 20) do
	end
		element33 = query("* marked:'checkIV'")
		touch(element33)
end

Then(/^I should see search page$/) do
	wait_poll(:until_exists => "* marked:'search_src_text'", :timeout => 20) do
	end
		puts element_exists("* marked:'search_src_text'")
		sleep(5)
end

# Following Settings

Then (/^I check if save is enabled or not$/)do
	wait_poll(:until_exists => "* marked:'proceedBtn'", :timeout => 20) do
	saveButtonAvailability = query("* marked:'proceedBtn'", :enabled).first
	puts "Save button enabled is : #{(saveButtonAvailability)}"
		while saveButtonAvailability == true	
		wait_poll(:until_exists => "* marked:'categorySelectedLayout'", :timeout => 40) do
		scroll("RecyclerView", :down)
		end
			selectedCategory = query("* marked:'categorySelectedLayout'").first
			touch(selectedCategory)
			saveButtonAvailability1 = query("* marked:'proceedBtn'", :enabled).first
			puts "Save button enabled is : #{(saveButtonAvailability1)}"
			sleep(2)
		end
	end
end

# Edit User Profile

Then(/^I click on edit button$/) do
    wait_poll(:until_exists => "* marked:'action_edit_profile'", :timeout => 20) do
    end
        element34 = query("* marked:'action_edit_profile'")
        touch(element34)
end

Then(/^I see edit profile page$/) do
    wait_poll(:until_exists => "* marked:'toolbar'", :timeout => 20) do
    end
        check_element_exists("* marked:'toolbar'")  
end  

Then(/^I add or edit Name$/) do
    wait_poll(:until_exists => "* marked:'userDescriptionET'", :timeout => 20) do 
	#query("userDescriptionET", :setText => "hello world")
    end
        # element35 = query("* marked:'userDescriptionET'")
        # touch(element35)
		# keyboard_enter_text 'Delete'
        # keyboard_enter_text("Hello World")
end

Then(/^I add or edit Introduction, Website name and title$/) do
    wait_poll(:until_exists => "* marked:'userTypeET'", :timeout => 40) do
	scroll("ScrollView", :down)
    end
		element36 = query("* marked:'userDescriptionET'")
		touch(element36)
		keyboard_enter_text("This is introduction")
		hide_soft_keyboard
		sleep (2)
		element37 = query("* marked:'userURLET'")
		touch(element37)
		keyboard_enter_text("www.google.com")
		hide_soft_keyboard
		sleep (2)
		element38 = query("* marked:'userTypeET'")
		touch(element38)
		keyboard_enter_text("This is title")
end