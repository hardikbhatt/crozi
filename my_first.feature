Feature: Menu feature    

Scenario: Open Home Menu
	Given the app has launched
	Then I press Home Menu button
	Then I press Home Button
	Then I should see Home Page
   
Scenario: Checking notification menu from sidebar
	Given the app has launched
	Then I press Home Menu button
	Then I should see User Profile Name
	Then I press Notifications Button
	Then I should see Notifications Page
   
Scenario: Checking Find Friends menu from sidebar
	Given the app has launched
	Then I press Home Menu button
	Then I press Find Friends Button
	Then I should see Find Friends Page
	Then I press Facebook Button
	Then I should see Facebook User 
   
Scenario: Checking Settings menu from sidebar
	Given the app has launched
	Then I press Home Menu button
	Then I should see User Profile Name
	Then I press Settings Button
	Then I should see Settings Page
   
Scenario: Open Home Page
	Given the app has launched
	Then I should see selected category as Everything
	Then I press category dropdown
	Then I press any category name
	Then I should see the selected category page
    
Scenario: Checking User story page
	Given the app has launched
	Then I press UserName
	Then I should see user story page
    
Scenario: Checking Like and comment functionality on user story page
    Given the app has launched
    Then I press UserName
    Then I see like count
    Then I press like button
    Then I check the like count 
    Then I press on comment icon
    Then I should see comment page
	Then I add the comment and press enter	
    Then the comment should get added
    
Scenario: Checking of story added to collection
	Given the app has launched
	Then I press collection
    Then I should see collection popup
    Then I press on create new collection icon
    Then I should see create new collection popup
    Then I add name of collection and press save
    Then I should see newly created collection
    Then I check which collection to select
    Then I press on done
    Then I press Home Menu button
    Then I press on user profile 
    Then I see user profile page
    Then I press on collection tab
    Then I see my user stories in collection
	
Scenario: Checking the other options available in story
    Given the app has launched
    Then I should click on other options
    Then I should see options popup
    Then I should click on Reports
    Then I should see Reports popup
    Then I should select any one reason to report and press on done
	
#Scenario: Create a Story
#	Given the app has launched
#	Then I should click on add item
#	Then I should click on add photo
#	Then I should click on add video
#	Then I should click on add embed
	
Scenario: Search Options
	Given the app has launched
    Then I should click on search
	Then I search and press enter
	Then I should see search page

Scenario: Following Settings
    Given the app has launched
    Then I press category dropdown
    Then I should click on setting
    Then I check if save is enabled or not

Scenario: Edit User Profile
	Given the app has launched
	Then I press Home Menu button
	Then I press on user profile 
	Then I see user profile page
	Then I click on edit button
	Then I see edit profile page
#	Then I add or edit Name
    Then I add or edit Introduction, Website name and title